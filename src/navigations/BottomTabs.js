import React from 'react';
import Home from '../screens/Home';
import ScanQR from '../screens/ScanQR';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const BottomTabs = () => {
  const Tab = createBottomTabNavigator();

  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        headerShown: false,
        tabBarIcon: ({size, color}) => {
          let iconName;

          if (route.name === 'Home') {
            iconName = 'home';
          } else if (route.name === 'Scan QR') {
            iconName = 'qrcode-scan';
          }
          return <Icon name={iconName} size={size} color={color} />;
        },
        tabBarActiveTintColor: '#0D28A6',
        tabBarInactiveTintColor: '#222222',
        tabBarStyle: {
          height: 60,
          borderTopLeftRadius: 20,
          borderTopRightRadius: 20,
        },
        tabBarItemStyle: {
          height: 40,
          alignSelf: 'center',
        },
      })}>
      <Tab.Screen name="Home" component={Home} />
      <Tab.Screen name="Scan QR" component={ScanQR} />
    </Tab.Navigator>
  );
};

export default BottomTabs;
