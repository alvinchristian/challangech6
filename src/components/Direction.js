import {TouchableOpacity, Text, StyleSheet} from 'react-native';
import React from 'react';

const Direction = ({navigation, caption, page}) => {
  return (
    <TouchableOpacity onPress={() => navigation.navigate(page)}>
      <Text style={styles.caption}>
        {caption}
        <Text style={{color: '#55CCF2'}}>{page}</Text>
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  caption: {
    fontFamily: 'Brandon_med',
    color: '#B4B8C4',
    fontSize: 15,
    marginTop: 50,
  },
});

export default Direction;
