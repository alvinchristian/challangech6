import {View, TextInput, TouchableOpacity, StyleSheet} from 'react-native';
import React, {useState} from 'react';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const Input = ({iconName, placeholder, onChangeText, secureTextEntry}) => {
  const [isSecureText, setIsSecureText] = useState(secureTextEntry);
  const [icon, setIcon] = useState('eye-off');
  return (
    <View style={styles.Container}>
      <Icon style={styles.Icon} name={iconName} size={22} color="#000" />
      <TextInput
        style={styles.Input}
        placeholder={placeholder}
        onChangeText={onChangeText}
        secureTextEntry={isSecureText}
      />
      {placeholder == 'Password' ? (
        <TouchableOpacity
          onPress={() => {
            setIsSecureText(prev => !prev);
            {
              isSecureText ? setIcon('eye') : setIcon('eye-off');
            }
          }}>
          <Icon style={styles.Icon2} name={icon} size={20} color="#000" />
        </TouchableOpacity>
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  Container: {
    backgroundColor: '#C3DCF4',
    width: '80%',
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 15,
    padding: 5,
    marginBottom: 20,
  },
  Icon: {
    marginVertical: 10,
    marginLeft: 10,
    paddingRight: 10,
    borderRightWidth: 3,
    borderColor: '#fff',
  },
  Input: {
    fontFamily: 'Brandon_med',
    marginHorizontal: 10,
    width: '65%',
    fontSize: 15,
  },
  Icon2: {
    marginVertical: 10,
    paddingLeft: 5,
  },
});

export default Input;
