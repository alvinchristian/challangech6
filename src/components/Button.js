import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import React from 'react';
import Icon from 'react-native-vector-icons/AntDesign';

const Button = ({bgColor, caption, marginTop, onPress}) => {
  return (
    <TouchableOpacity
      style={{
        ...styles.Container,
        backgroundColor: bgColor,
        marginTop: marginTop,
      }}
      onPress={onPress}>
      {caption == 'Google' ? (
        <Icon style={styles.Icon} name={'google'} size={20} color="#fff" />
      ) : null}
      <Text style={styles.Text}>{caption}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  Container: {
    flexDirection: 'row',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    width: 150,
  },
  Icon: {
    marginRight: 10,
  },
  Text: {
    fontFamily: 'Brandon_bld',
    fontSize: 16,
    color: '#fff',
  },
});

export default Button;
