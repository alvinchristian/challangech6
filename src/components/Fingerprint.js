import {TouchableOpacity, Alert, StyleSheet} from 'react-native';
import React from 'react';
import TouchID from 'react-native-touch-id';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import BottomTabs from '../navigations/BottomTabs';

const Fingerprint = ({navigation}) => {
  const optionalConfigObject = {
    title: 'Authentication Required',
    color: '#e00606',
    fallbackLabel: 'Show Passcode',
  };

  const pressHandler = () => {
    TouchID.authenticate('Login Authenticaiton', optionalConfigObject)
      .then(success => {
        navigation.replace('BottomTabs');
      })
      .catch(error => {
        Alert.alert('ERROR', error);
      });
  };

  return (
    <TouchableOpacity style={styles.Container} onPress={() => pressHandler()}>
      <Icon style={styles.Icon} name={'qrcode-scan'} size={20} color="#fff" />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  Container: {
    width: 50,
    height: 40,
    backgroundColor: 'black',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    marginTop: 10,
    marginLeft: 10,
  },
});

export default Fingerprint;
