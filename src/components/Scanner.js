import React from 'react';
import {Alert} from 'react-native';
import {CameraScreen, CameraType} from 'react-native-camera-kit';
import {useIsFocused} from '@react-navigation/native';

const Scanner = () => {
  const isFocused = useIsFocused();
  const onReadCode = event =>
    Alert.alert('QR CODE', event.nativeEvent.codeStringValue);

  return isFocused ? (
    <CameraScreen
      cameraType={CameraType.Back}
      scanBarcode={true}
      onReadCode={event => onReadCode(event)}
      showFrame={true}
      laserColor={'red'}
      frameColor={'white'}
    />
  ) : null;
};

export default Scanner;
