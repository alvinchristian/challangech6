import {StyleSheet, Alert} from 'react-native';
import React, {useEffect, useState} from 'react';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';
import {request, check, PERMISSIONS, RESULTS} from 'react-native-permissions';

Geolocation.setRNConfiguration();

const Maps = () => {
  const [position, setPosition] = useState({
    lat: 37.78825,
    long: -122.4324,
  });

  const permission = async () => {
    try {
      await request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION);
      const result = await check(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION);
      result == RESULTS.DENIED
        ? Alert.alert(
            'WARNING',
            'Location Permission Denied! Please Allow Permission Request!',
          )
        : currentPosition();
    } catch (error) {
      Alert.alert('ERROR', error);
    }
  };

  const currentPosition = () => {
    Geolocation.getCurrentPosition(info => {
      setPosition({
        lat: info.coords.latitude,
        long: info.coords.longitude,
      });
    });
  };

  useEffect(() => {
    permission();
  }, []);

  return (
    <MapView
      provider={PROVIDER_GOOGLE} // remove if not using Google Maps
      style={styles.map}
      region={{
        latitude: position.lat,
        longitude: position.long,
        latitudeDelta: 0.015,
        longitudeDelta: 0.0121,
      }}
      showsUserLocation
      showsMyLocationButton
      followsUserLocation>
      <Marker
        coordinate={{
          latitude: -0.9215345269489723,
          longitude: 119.88405289516469,
        }}
        title="Kopi Janji Jiwa"
        description="Coffee Shop and Laundry"></Marker>
      <Marker
        coordinate={{
          latitude: -0.919148555121568,
          longitude: 119.88699756539225,
        }}
        title="Best Western Plus Coco Palu"
        description="3 Stars Hotel"></Marker>
      <Marker
        coordinate={{
          latitude: -0.9191941324338314,
          longitude: 119.88499217757393,
        }}
        title="Bank BCA"
        description="Bank and ATM"></Marker>
      <Marker
        coordinate={{
          latitude: -0.9187040705454498,
          longitude: 119.87974282310286,
        }}
        title="Grand Hero"
        description="Swalayan"></Marker>
      <Marker
        coordinate={{
          latitude: -0.92027499776566,
          longitude: 119.87792757507064,
        }}
        title="SPBU Towua"
        description="Pertamina"></Marker>
    </MapView>
  );
};

const styles = StyleSheet.create({
  map: {
    ...StyleSheet.absoluteFillObject,
    width: ' 100%',
    height: ' 100%',
  },
});

export default Maps;
