import {StatusBar, SafeAreaView, StyleSheet} from 'react-native';
import React, {useEffect} from 'react';
import Header from '../components/Header';
import Maps from '../components/Maps';
import analytics from '@react-native-firebase/analytics';

const Home = ({navigation}) => {
  const onHomeScreenView = async () => {
    await analytics().logScreenView({
      screen_name: 'Home',
      screen_class: 'Home',
    });
  };

  useEffect(() => {
    onHomeScreenView();
  }, []);

  return (
    <SafeAreaView style={styles.Container}>
      <StatusBar backgroundColor={'#fff'} barStyle={'dark-content'} />
      <Header navigation={navigation} />
      <Maps />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});

export default Home;
