import {
  Text,
  SafeAreaView,
  KeyboardAvoidingView,
  StatusBar,
  StyleSheet,
  Platform,
  Alert,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import Input from '../components/Input';
import Button from '../components/Button';
import Direction from '../components/Direction';
import auth from '@react-native-firebase/auth';
import analytics from '@react-native-firebase/analytics';

const SignUp = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const handleSignUp = async () => {
    try {
      if (email != '' && password != '') {
        await auth().createUserWithEmailAndPassword(email, password);
        Alert.alert('SUCCESS', 'Account created & log in successful!');
        navigation.replace('BottomTabs');
      } else {
        Alert.alert('WARNING', 'Please input your email and password!');
      }
    } catch (error) {
      switch (error.code) {
        case 'auth/email-already-in-use':
          Alert.alert('WARNING', 'That email address is already in use!');
          break;
        case 'auth/invalid-email':
          Alert.alert('WARNING', 'That email address is invalid!');
          break;
        case 'auth/weak-password':
          Alert.alert('WARNING', 'That password is invalid!');
          break;
        case 'auth/too-many-requests':
          Alert.alert('WARNING', 'Too many request, please try again later!');
          break;
      }
    }
  };

  const onSignUpScreenView = async () => {
    await analytics().logScreenView({
      screen_name: 'SignUp',
      screen_class: 'SignUp',
    });
  };

  useEffect(() => {
    onSignUpScreenView();
  }, []);

  return (
    <SafeAreaView style={styles.Main}>
      <KeyboardAvoidingView
        style={styles.Container}
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
        <StatusBar backgroundColor={'#fff'} barStyle={'dark-content'} />
        <Text style={styles.Title}>My App</Text>
        <Input
          iconName={'account'}
          placeholder={'Email'}
          onChangeText={text => setEmail(text)}
        />
        <Input
          iconName={'lock'}
          placeholder={'Password'}
          onChangeText={text => setPassword(text)}
          secureTextEntry={true}
        />
        <Button
          caption={'SIGN UP'}
          bgColor={'#000'}
          marginTop={10}
          onPress={handleSignUp}
        />
        <Direction
          navigation={navigation}
          caption={'Join us before? '}
          page={'Login'}
        />
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  Main: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
  },
  Container: {
    alignItems: 'center',
  },
  Title: {
    fontFamily: 'Brandon_bld',
    fontSize: 40,
    marginBottom: 40,
    color: '#000',
  },
  New: {
    marginTop: 50,
  },
});

export default SignUp;
