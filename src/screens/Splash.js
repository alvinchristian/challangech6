import {StyleSheet, Text, View, StatusBar, Image} from 'react-native';
import React, {useEffect, useState} from 'react';
import {useNavigation} from '@react-navigation/native';
import auth from '@react-native-firebase/auth';

const Splash = () => {
  const [user, setUser] = useState('');

  const navigation = useNavigation();

  useEffect(() => {
    const unsubscribe = auth().onAuthStateChanged(user => {
      setUser(user);
      const isLogin = user ? 'BottomTabs' : 'Login';
      setTimeout(() => {
        navigation.replace(isLogin);
      }, 2000);
    });
    return unsubscribe;
  }, [user]);

  return (
    <View style={styles.Container}>
      <StatusBar backgroundColor="#fff" barStyle="dark-content" />
      <Text style={styles.Title}>My App</Text>
      <Text style={styles.MyName}>Alvin Christian</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingTop: '40%',
    paddingBottom: '10%',
  },
  Title: {
    fontFamily: 'Brandon_bld',
    fontSize: 50,
    marginBottom: 40,
    color: '#000',
  },
  MyName: {
    fontSize: 14,
    fontFamily: 'Brandon_bld',
    fontWeight: 'bold',
    color: '#000',
  },
});

export default Splash;
