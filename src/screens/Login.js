import {
  View,
  Text,
  SafeAreaView,
  KeyboardAvoidingView,
  StatusBar,
  Alert,
  StyleSheet,
  Platform,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import Input from '../components/Input';
import Button from '../components/Button';
import Direction from '../components/Direction';
import CrashButton from '../components/CrashButton';
import Fingerprint from '../components/Fingerprint';
import auth from '@react-native-firebase/auth';
import {GoogleSignin} from '@react-native-google-signin/google-signin';
import analytics from '@react-native-firebase/analytics';
import TouchID from 'react-native-touch-id';

const Login = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [touchID, setTouchID] = useState(true);

  GoogleSignin.configure({
    webClientId:
      '356676962748-u3jnd5knd67u3dgdrhri5o2tum3q5k51.apps.googleusercontent.com',
  });

  const isSupported = () => {
    TouchID.isSupported()
      .then(biometryType => {
        if (biometryType === true || biometryType === 'TouchID') {
          setTouchID(true);
        } else {
          setTouchID(false);
        }
      })
      .catch(error => {
        Alert.alert('ERROR', error);
      });
  };

  const handleLogIn = async () => {
    try {
      if (email != '' && password != '') {
        await auth().signInWithEmailAndPassword(email, password);
        await analytics().logEvent('user_login', {
          email: email,
        });
        Alert.alert('SUCCESS', 'Login Successful!');
        navigation.replace('BottomTabs');
      } else {
        Alert.alert('WARNING', 'Please input your email and password!');
      }
    } catch (error) {
      switch (error.code) {
        case 'auth/user-not-found':
          Alert.alert(
            'WARNING',
            "The email address you entered isn't connected to an account!",
          );
          break;
        case 'auth/invalid-email':
          Alert.alert('WARNING', 'That email address is invalid!');
          break;
        case 'auth/wrong-password':
          Alert.alert('WARNING', 'That password is uncorrect!');
          break;
        case 'auth/too-many-requests':
          Alert.alert('WARNING', 'Too many request, please try again later!');
          break;
      }
    }
  };

  const handleGoogleLogin = async () => {
    const {idToken} = await GoogleSignin.signIn();

    // Create a Google credential with the token
    const googleCredential = auth.GoogleAuthProvider.credential(idToken);
    // Sign-in the user with the credential
    await auth().signInWithCredential(googleCredential);
    Alert.alert('SUCCESS', 'Login Successful!');
    navigation.replace('BottomTabs');
  };

  const onLoginScreenView = async () => {
    await analytics().logScreenView({
      screen_name: 'Login',
      screen_class: 'Login',
    });
  };

  useEffect(() => {
    onLoginScreenView();
    isSupported();
  }, []);

  return (
    <SafeAreaView style={styles.Main}>
      <KeyboardAvoidingView
        style={styles.Container}
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
        <StatusBar backgroundColor={'#fff'} barStyle={'dark-content'} />
        <CrashButton />
        <Text style={styles.Title}>My App</Text>
        <Input
          iconName={'account'}
          placeholder={'Email'}
          onChangeText={text => setEmail(text)}
        />
        <Input
          iconName={'lock'}
          placeholder={'Password'}
          onChangeText={text => setPassword(text)}
          secureTextEntry={true}
        />
        <View style={styles.Button}>
          <Button
            caption={'LOG IN'}
            bgColor={'#000'}
            marginTop={10}
            onPress={handleLogIn}
          />
          {touchID ? <Fingerprint navigation={navigation} /> : null}
        </View>
        <Text style={styles.Separator}>- Or -</Text>
        <Button
          caption={'Google'}
          bgColor={'#F24336'}
          onPress={handleGoogleLogin}
        />
        <Direction
          navigation={navigation}
          caption={"Don't have an account? "}
          page={'SignUp'}
        />
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  Main: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
  },
  Container: {
    alignItems: 'center',
  },
  Title: {
    fontFamily: 'Brandon_bld',
    fontSize: 40,
    marginBottom: 40,
    color: '#000',
  },
  Button: {
    flexDirection: 'row',
  },
  Separator: {
    fontFamily: 'Brandon_blk',
    color: '#000',
    fontSize: 15,
    marginVertical: 10,
  },
});

export default Login;
